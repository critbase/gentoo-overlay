# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Simple, low-latency game streaming"
HOMEPAGE="https://parsecgaming.com"
SRC_URI="https://s3.amazonaws.com/parsec-build/package/parsec-linux.deb"

LICENSE="all-rights-reserved"
SLOT="0"
KEYWORDS="~amd64"
RESTRICT="mirror bindist"

RDEPEND="
	sys-devel/binutils
	sys-libs/glibc
	x11-libs/cairo
	media-libs/freetype
	sys-devel/gcc
	x11-libs/gdk-pixbuf
	virtual/opengl
	virtual/libstdc++
	x11-libs/gtk+:2
	x11-libs/pango
	x11-libs/libSM
	x11-libs/libX11
	x11-libs/libXxf86vm"
DEPEND=""
BDEPEND=""
