# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=6

PYTHON_COMPAT=( python3_6 )

inherit vala cmake-utils python-r1 gnome2-utils

DESCRIPTION="A simple, focused eBook reader"
HOMEPAGE="https://babluboy.github.io/bookworm"
SRC_URI="https://github.com/babluboy/bookworm/archive/${PV}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND="
	${PYTHON_DEPS}
	$(vala_depend)
	dev-libs/vala-common
	dev-libs/granite
	net-libs/webkit-gtk:4/37
	dev-db/sqlite:3
	app-text/poppler[utils]
	dev-python/html2text[${PYTHON_USEDEP}]
	net-misc/curl
	app-arch/unzip
	app-arch/unar
	x11-libs/gtk+:3
	dev-libs/libxml2
	dev-libs/libgee:0.8/2"
RDEPEND="${DEPEND}"
BDEPEND=""

src_prepare() {
	export VALAC="valac-$(vala_best_api_version)"
	vala_src_prepare
	cmake-utils_src_prepare
}

src_configure() {
	local mycmakeargs=(
		-DICON_UPDATE=OFF
		-DGSETTINGS_COMPILE=OFF
		-DVALA_EXECUTABLE="${VALAC}"
	)
	cmake-utils_src_configure
}

pkg_postinst() {
	gnome2_schemas_update
	xdg_desktop_database_update
}

pkg_postrm() {
	gnome2_schemas_update
	xdg_desktop_database_update
}
